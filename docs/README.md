# USAGE #

Postbox exposes two sets of gRPC endpoints that can be used to submit and retrieve CIM-based data. We recommend using Zepben's [cimbend](TODO) library to communicate with postbox.

## Connecting ##
Authentication

To connect to an SSL/TLS Postbox server at minimum you will require a copy of the servers CA certificate.
The server can also be configured to require client authentication, in which case you will also have to provide a signed certificate and key pair, signed by a CA the server recognises.

On top of SSL/TLS, Postbox also requires OAuth2 token based authentication. To configure your token you must supply `CallCredential`'s to your secure channel (do not use tokens on an insecure channel).

To create an insecure channel:
    
    channel = grpc.insecure_channel("<host>:<port>")
        
To create a secure channel:

    call_credentials = grpc.access_token_call_credentials(TOKEN)
    # Channel credential will be valid for the entire channel
    channel_credentials = grpc.ssl_channel_credentials(CA_CERT, KEY, CERT)
    # Combining channel credentials and call credentials together
    composite_credentials = grpc.composite_channel_credentials(
        channel_credentials,
        call_credentials,
    )
    channel = grpc.secure_channel(addr, composite_credentials)

TOKEN should be your OAuth2 token as provided by the relevant OAuth2 auth provider (see your service docs for details)
CA_CERT should be the binary content of the CA certificate chain file. I.e:

    with open(path_to_ca_cert, 'rb') as f:
        CA_CERT = f.read()

KEY and CERT should be the binary content of your clients private key and signed certificate.

    with open(path_to_cert, 'rb') as cert:
        CERT = cert.read()
    with open(path_to_key, 'rb') as key:
        KEY = key.read()
   
A context manager like below can be useful to create gRPC channels.

    import contextlib
    
    @contextlib.contextmanager
    def create_client_channel(addr, secure=False, token=None, ca=None, key=None, cert=None):
        if secure:
            call_credentials = grpc.access_token_call_credentials(token)
            # Channel credential will be valid for the entire channel
            channel_credentials = grpc.ssl_channel_credentials(ca, key, cert)
            # Combining channel credentials and call credentials together
            composite_credentials = grpc.composite_channel_credentials(
                channel_credentials,
                call_credentials,
            )
            channel = grpc.secure_channel(addr, composite_credentials)
        else:
            channel = grpc.insecure_channel(addr)

        yield channel

## Sending data ##

To send data to postbox it's recommended to use a Zepben network model library. Currently this means [cimbend](TODO). See the documentation of cimbend for how this works.

This version of postbox is currently tied to cimbend 0.1. 

The definition for the postbox RPC's can be found [here](TODO)

Postbox currently has RPC's that accept the following types from cimbend (including all their attributes and sub-fields):

    BaseVoltage
    AssetInfo - CableInfo, OverheadWireInfo, TransformerEndInfo
    PerLengthSequenceImpedance
    EnergySource
    EnergyConsumer
    PowerTransformer 
    ACLineSegment
    Breaker
    Meter
    MeterReading
    UsagePoint
    Customer
    
It also imposes the following dependencies, meaning you must send dependants beforehand, otherwise the server will respond with an error:

    Everything that extends ConductingEquipment requires a BaseVoltage
    ACLineSegment requires PerLengthSequenceImpedance, AssetInfo [CableInfo|OverheadWireInfo]
    Meter requires UsagePoint
    UsagePoint requires all relevant ConductingEquipment
    MeterReading requires Meter
    
See Sending examples below or in cimbend for how to convert objects to protobuf and send to the server.

## Fetching data ##
At the moment only a basic RPC call that fetches the entire network is available. 
`getWholeNetwork()` Takes an Empty and returns a stream of `Equipment`. Any field in `Equipment` can be returned and should be handled.

To query, use the following:

    from zepben.postbox import Identity
    nds = NetworkDataStub(channel)
    stream = nds.getWholeNetwork(Identity())
    for eq_msg in stream:
        if eq_msg.HasField("<field name>")
            ... do something with field ...
            

### Example: Sending a PowerTransformer ###

    from zepben.model import Terminal, PowerTransformer, PowerTransformerEnd, RatioTapChanger,                            PhaseCode, VectorGroup, WindingConnection
    from zepben.postbox import NetworkDataStub

Create the Stub for sending the objects:

    nds = NetworkDataStub(channel)
    
Create the type with your data, this example includes RatioTapChanger's, Terminal's, and PowerTransformerEnd's with the PowerTransformer:

    # Order of terminals matters, as it is used to index into other components. For example,
    # a terminal can be represented on a map with PositionPoint's that are also associated 
    # with an ACLineSegment, and each Terminal should share the same index as its 
    # corresponding PositionPoint.
    terms = []
    terms.append(Terminal(mrid="39f0109a-0846-4ed7-870a-76c01b7f757a", PhaseCode.ABC, connectivity_node="f7689f0d-e73d-4922-9109-7ff428699510"))
    terms.append(Terminal(mrid="45b3cb51-10a8-41a7-9021-82a0b5d30b78", PhaseCode.ABC, connectivity_node="2ac83e5e-3a98-4f03-b822-45d40eb02f30"))
    
    tp1 = RatioTapChanger(high_step=3, low_step=1, step=0.5, step_voltage_increment=0.5)
    tp2 = RatioTapChanger(high_step=3, low_step=1, step=0.5, step_voltage_increment=0.5)
    end1 = PowerTransformerEnd(rated_s=3.0, rated_u=22000.0, r=30.0, x=20.0, r0=15.0, x0=10.0, winding=WindingConnection.D, tap_changer=tp1)
    end2 = PowerTransformerEnd(rated_s=3.0, rated_u=11000.0, r=10.0, x=5.0, r0=7.0, x0=5.0, winding=WindingConnection.Y, tap_changer=tp2)
     
    # Create the PowerTransformer
    pt = PowerTransformer(mrid="3f5ccd6d-dcd9-4b0a-a3e7-e98acf865562", name="Transformer X", vector_group=VectorGroup.DYN11, in_service=True, ends=[end1, end2], terminals=terms)
    
    # Send to server. the to_pb() call will do the heavy lifting and convert everything 
    # to its protobuf representation.
    nds.createPowerTransformer(pt.to_pb())

### Example: Sending an ACLineSegment ###
Import the classes

    from zepben.model import Terminal, BaseVoltage, PerLengthSequenceImpedance, ACLineSegment, PhaseCode, OverheadWireInfo, WireMaterialKind
    from zepben.postbox import NetworkDataStub

Create the Stub for sending the objects:

    nds = NetworkDataStub(channel)
    
Create an instance of the type with your data:

    # Order of terminals matters, as it is used to index into other components. For example,
    # a terminal can be represented on a map with PositionPoint's that are also associated 
    # with an ACLineSegment, and each Terminal should share the same index as its 
    # corresponding PositionPoint.
    terms = []
    terms.append(Terminal(mrid="39f0109a-0846-4ed7-870a-76c01b7f757a", PhaseCode.ABC, connectivity_node="f7689f0d-e73d-4922-9109-7ff428699510"))
    terms.append(Terminal(mrid="45b3cb51-10a8-41a7-9021-82a0b5d30b78", PhaseCode.ABC, connectivity_node="2ac83e5e-3a98-4f03-b822-45d40eb02f30"))
    
    # Create and send dependencies.
    bv = BaseVoltage(mrid="ad033d84-6362-4bc1-aadc-eae18b1135d9", nom_volt=22000)
    plsi = PerLengthSequenceImpedance(mrid="ae670ba6-adee-45eb-a0ec-1174f3ba2ba4", r=10.0, x=100.0, r0=11.0, x0=101.0, bch=2.5435, b0ch=1.434)
    nds.createBaseVoltage(bv.to_pb())
    nds.createPerLengthSequenceImpedance(plsi.to_pb())
    
    # Create and send the ACLS
    acls = ACLineSegment(mrid="3f5ccd6d-dcd9-4b0a-a3e7-e98acf865562", plsi=plsi, length=1234, base_voltage=bv, inService=True, terminals=terms) 
    nds.createAcLineSegment(acls.to_pb())
