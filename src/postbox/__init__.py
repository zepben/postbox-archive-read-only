"""
Copyright 2019 Zeppelin Bend Pty Ltd
This file is part of postbox.

postbox is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

postbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with postbox.  If not, see <https://www.gnu.org/licenses/>.
"""


write_network_scope = 'write:network'
read_network_scope = 'read:network'
write_metrics_scope = 'write:metrics'


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class NoEquipmentException(Exception):
    pass
