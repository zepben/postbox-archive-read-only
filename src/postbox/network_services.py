"""
Copyright 2019 Zeppelin Bend Pty Ltd
This file is part of postbox.

postbox is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

postbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with postbox.  If not, see <https://www.gnu.org/licenses/>.
"""


import logging
import zepben.postbox.pb_pb2_grpc as pb_grpc
from grpc import StatusCode
from google.protobuf.any_pb2 import Any
from zepben.model import EnergySource as CIMEnergySource, EnergyConsumer as CIMEnergyConsumer,  \
    PowerTransformer as CIMPowerTransformer, ACLineSegment as CIMACLineSegment, Breaker as CIMBreaker, ConnectivityNode
from zepben.cim.iec61970 import EnergySource
from zepben.cim.iec61968 import AssetInfo as PBAssetInfo
from zepben.model.exceptions import MissingReferenceException
from zepben.postbox.pb_pb2 import ACLSResponse, BreakerResponse, ECResponse, ESResponse, PTResponse, Equipment, \
    PLSIResponse, BVResponse, AIResponse, MeterResponse, UPResponse, DOResponse, SubstationResponse
from zepben.postbox.pb_pb2 import Summary

log = logging.getLogger(__name__)


class MissingFieldException(Exception):
    pass


def validate_id(resource):
    """
    Ensure every message has an associated network ID and a resource ID (mRID)
    :param resource: A resource containing an IdentityObject (at least a network ID and an mRID)
    :return:
    """
    if not resource.mRID:
        raise MissingFieldException(f"mRID field was not defined and is a required field - Resource received was: {resource}")


def convert_equip(e):
    if isinstance(e, CIMACLineSegment):
        equip = Equipment(acls=e.to_pb())
    elif isinstance(e, CIMPowerTransformer):
        equip = Equipment(pt=e.to_pb())
    elif isinstance(e, CIMEnergyConsumer):
        equip = Equipment(ec=e.to_pb())
    elif isinstance(e, CIMBreaker):
        equip = Equipment(br=e.to_pb())
    elif isinstance(e, CIMEnergySource):
        equip = Equipment(es=e.to_pb())
    elif isinstance(e, ConnectivityNode):
        return None
    else:
        # Unknown type, try convert to Protobuf
        try:
            eq = e.to_pb()
        except:
            eq = e
        # TODO: Test this
        # Pack it as an any in the other field
        equip = Equipment(other=Any().Pack(eq))
    return equip


def _handle_missing_ref(exc, context):
    context.set_details(exc.info)
    context.set_code(StatusCode.UNKNOWN)


class NetworkDataServicer(pb_grpc.NetworkDataServicer):
    def __init__(self, network):
        super().__init__()
        self._network = network

    @property
    def network(self):
        return self._network

    def createEnergySource(self, request, context):
        try:
            validate_id(request)
        except MissingFieldException as mie:
            context.set_details(str(mie))
            context.set_code(StatusCode.UNKNOWN)
            return ESResponse()

        try:
            self.network.add(request)
        except MissingReferenceException as mre:
            _handle_missing_ref(mre, context)
        except Exception as e:
            # Capture all exceptions and treat them as internal errors - we don't want to expose error details to clients here
            context.set_details(f"Internal Server Error - please report this issue. ID was {request.mRID}")
            log.error(f"Error occured while adding energy source: {e}", exc_info=True)
            context.set_code(StatusCode.INTERNAL)
            return ESResponse()

        return ESResponse()

    def createSubstation(self, request, context):
        try:
            validate_id(request)
        except MissingFieldException as mie:
            context.set_details(str(mie))
            context.set_code(StatusCode.UNKNOWN)
            return ESResponse()

        try:
            self.network.add(request.substation)
        except MissingReferenceException as mre:
            _handle_missing_ref(mre, context)
        except Exception as e:
            # Capture all exceptions and treat them as internal errors - we don't want to expose error details to clients here
            context.set_details(f"Internal Server Error - please report this issue. ID was {request.mRID}")
            log.error(f"Error occured while adding energy source: {e}", exc_info=True)
            context.set_code(StatusCode.INTERNAL)
            return SubstationResponse()

        return SubstationResponse()

    def createEnergyConsumer(self, request, context):
        try:
            validate_id(request)
        except MissingFieldException as mie:
            context.set_details(str(mie))
            context.set_code(StatusCode.UNKNOWN)
            return ECResponse()

        try:
            self.network.add(request)
        except MissingReferenceException as mre:
            _handle_missing_ref(mre, context)
        except Exception as e:
            # Capture all exceptions and treat them as internal errors - we don't want to expose error details to clients here
            context.set_details(f"Internal Server Error - please report this issue. ID was {request.mRID}")
            log.error(f"Error occured while adding Energy Consumer: {e}", exc_info=True)
            context.set_code(StatusCode.INTERNAL)
            return ECResponse()

        return ECResponse()

    def createPowerTransformer(self, request, context):
        try:
            validate_id(request)
        except MissingFieldException as mie:
            context.set_details(str(mie))
            context.set_code(StatusCode.UNKNOWN)
            return PTResponse()

        try:
            self.network.add(request)
        except MissingReferenceException as mre:
            _handle_missing_ref(mre, context)
        except Exception as e:
            # Capture all exceptions and treat them as internal errors - we don't want to expose error details to clients here
            context.set_details(f"Internal Server Error - please report this issue. ID was {request.mRID}")
            log.error(f"Error occured while adding Power Transformer: {e}", exc_info=True)
            context.set_code(StatusCode.INTERNAL)
            return PTResponse()

        return PTResponse()

    def createAcLineSegment(self, request, context):
        try:
            validate_id(request)
        except MissingFieldException as mie:
            context.set_details(str(mie))
            context.set_code(StatusCode.UNKNOWN)
            return ACLSResponse()

        try:
            self.network.add(request)
        except MissingReferenceException as mre:
            _handle_missing_ref(mre, context)
        except Exception as e:
            # Capture all exceptions and treat them as internal errors - we don't want to expose error details to clients here
            context.set_details(f"Internal Server Error - please report this issue. ID was {request.mRID}")
            log.error(f"Error occured while adding AcLineSegment: {e}", exc_info=True)
            context.set_code(StatusCode.INTERNAL)
            return ACLSResponse()

        return ACLSResponse()

    def createDiagramObject(self, request, context):
        # TODO: reimplement the network add. Ideally we should support _only_ receiving DiagramObjects
        context.set_details(f"Not Implemented")
        log.info(f"Received DiagramObject but this is not supported. ignoring")
        context.set_code(StatusCode.UNIMPLEMENTED)
        return DOResponse()

    def createBreaker(self, request, context):
        try:
            validate_id(request)
        except MissingFieldException as mie:
            context.set_details(str(mie))
            context.set_code(StatusCode.UNKNOWN)
            return BreakerResponse()

        try:
            self.network.add(request)
        except MissingReferenceException as mre:
            _handle_missing_ref(mre, context)
        except Exception as e:
            # Capture all exceptions and treat them as internal errors - we don't want to expose error details to clients here
            context.set_details(f"Internal Server Error - please report this issue. ID was {request.mRID}")
            log.error(f"Error occured while adding Breaker: {e}", exc_info=True)
            context.set_code(StatusCode.INTERNAL)
            return BreakerResponse()

        return BreakerResponse()

    def createPerLengthSequenceImpedance(self, request, context):
        """Submit a PerLengthSequenceImpedance"""
        try:
            validate_id(request)
        except MissingFieldException as mie:
            context.set_details(str(mie))
            context.set_code(StatusCode.UNKNOWN)
            return PLSIResponse()

        try:
            self.network.add(request)
        except Exception as e:
            # Capture all exceptions and treat them as internal errors - we don't want to expose error details to clients here
            context.set_details(f"Internal Server Error - please report this issue. ID was {request.mRID}")
            log.error(f"Error occured while adding PerLengthSequenceImpedance: {e}", exc_info=True)
            context.set_code(StatusCode.INTERNAL)
            return PLSIResponse()

        return PLSIResponse()

    def createBaseVoltage(self, request, context):
        """Submit a BaseVoltage"""
        try:
            validate_id(request)
        except MissingFieldException as mie:
            context.set_details(str(mie))
            context.set_code(StatusCode.UNKNOWN)
            return BVResponse()

        try:
            self.network.add(request)
        except Exception as e:
            # Capture all exceptions and treat them as internal errors - we don't want to expose error details to clients here
            context.set_details(f"Internal Server Error - please report this issue. ID was {request.mRID}")
            log.error(f"Error occured while adding BaseVoltage: {e}", exc_info=True)
            context.set_code(StatusCode.INTERNAL)
            return BVResponse()

        return BVResponse()

    def createMeter(self, request, context):
        """Submit a Meter"""
        try:
            self.network.add(request.meter)
        except MissingReferenceException as mre:
            _handle_missing_ref(mre, context)
        except Exception as e:
            # Capture all exceptions and treat them as internal errors - we don't want to expose error details to clients here
            context.set_details(f"Internal Server Error - please report this issue. ID was {request.mRID}")
            log.error(f"Error occured while adding Meter: {e}", exc_info=True)
            context.set_code(StatusCode.INTERNAL)
            return MeterResponse()

        return MeterResponse()

    def createUsagePoint(self, request, context):
        """Submit a Meter"""
        try:
            self.network.add(request.usagePoint)
        except MissingReferenceException as mre:
            _handle_missing_ref(mre, context)
        except Exception as e:
            # Capture all exceptions and treat them as internal errors - we don't want to expose error details to clients here
            context.set_details(f"Internal Server Error - please report this issue. ID was {request.mRID}")
            log.error(f"Error occured while adding UsagePoint: {e}", exc_info=True)
            context.set_code(StatusCode.INTERNAL)
            return UPResponse()

        return UPResponse()

    def createAssetInfo(self, request, context):
        """Submit an AssetInfo"""
        try:
            self.network.add(request.assetInfo)
        except Exception as e:
            # Capture all exceptions and treat them as internal errors - we don't want to expose error details to clients here
            context.set_details(f"Internal Server Error - please report this issue. ID was {request.mRID}")
            log.error(f"Error occured while adding AssetInfo: {e}", exc_info=True)
            context.set_code(StatusCode.INTERNAL)
            return AIResponse()

        return AIResponse()

    def getEnergySource(self, request, context):
        try:
            validate_id(request)
        except MissingFieldException as mie:
            context.set_details(str(mie))
            context.set_code(StatusCode.UNKNOWN)
            return Summary()
        equip = self.network.get_equipment(request.mRID)
        if isinstance(EnergySource, equip):
            return equip
        else:
            context.set_details("No EnergySource with mRID {request.mRID} found.")
            context.set_code(StatusCode.NOT_FOUND)
            return EnergySource()

    def dumpEjson(self, request, context):
        #self.network.network_to_ejson()
        return Summary()

    def traceDirection(self, request, context):
        #self.network.trace_and_set_directions()
        #self.network.dumpTracing()
        return Summary()

    def getWholeNetwork(self, request, context):
        for mrid, e in self.network.base_voltages.items():
            equip = Equipment(bv=e.to_pb())
            if equip:
                yield equip

        for mrid, e in self.network.seq_impedances.items():
            equip = Equipment(si=e.to_pb())
            if equip:
                yield equip

        for mrid, e in self.network.asset_infos.items():
            equip = Equipment(ai=PBAssetInfo(overheadWireInfo=e.to_pb()))
            if equip:
                yield equip

        for mrid, e in self.network.lines.items():
            equip = convert_equip(e)
            if equip:
                yield equip

        for mrid, e in self.network.energy_consumers.items():
            equip = convert_equip(e)
            if equip:
                yield equip

        for mrid, e in self.network.energy_sources.items():
            equip = convert_equip(e)
            if equip:
                yield equip

        for mrid, e in self.network.transformers.items():
            equip = convert_equip(e)
            if equip:
                yield equip

        for mrid, e in self.network.breakers.items():
            equip = convert_equip(e)
            if equip:
                yield equip

        # Usage points depend on resources being sent already
        for mrid, e in self.network.usage_points.items():
            equip = Equipment(up=e.to_pb())
            if equip:
                yield equip

        for mrid, e in self.network.meters.items():
            equip = Equipment(mt=e.to_pb())
            if equip:
                yield equip

    def getEquipmentContainer(self, request, context):
        # TODO: Send catalogs first
        for e in self.network.depth_first_trace_and_apply():
            equip = convert_equip(e)
            yield equip

