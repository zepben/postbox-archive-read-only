"""
Copyright 2019 Zeppelin Bend Pty Ltd
This file is part of postbox.

postbox is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

postbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with postbox.  If not, see <https://www.gnu.org/licenses/>.
"""


import argparse
import asyncio
import threading
import contextlib
import grpc
import logging
import time
import postbox.auth as auth
import zepben.postbox.pb_pb2_grpc as pb_grpc
from concurrent import futures
from postbox.network_services import NetworkDataServicer
from postbox.metrics_services import MeterReadingsServicer
from zepben.model import MetricsStore, Network
from postbox.config import load_config, LogFormatter, CONFIG_FILE_LOCATIONS

_ONE_DAY_IN_SECONDS = 60 * 60 * 24
_DEFAULT_BUCKET_DURATION = 5000
log = logging.getLogger(__package__)
handler = logging.StreamHandler()
handler.setFormatter(LogFormatter())
log.setLevel(logging.DEBUG)
log.addHandler(handler)


@contextlib.contextmanager
def serve(config):
    """
    Run the gRPC server. Will launch a secure server (HTTPS) with client verification.
    config must define a key, cert, and root_certificate where:
      key and cert must be signed by a trusted CA.
      root_certificate must be the CA cert which was used to sign the clients certificates.
    :param config: `postbox.config.Config`
    :return:
    """
    if config.ssl_enabled:
        log.info(f"Using Cert: {config.cert}, Key: {config.key}")
        server_cert = auth.load_credential_from_file(config.cert)
        server_key = auth.load_credential_from_file(config.key)
        ca_chain = auth.load_credential_from_file(config.root_certificate)
        server_creds = grpc.ssl_server_credentials(
            ((server_key, server_cert),),
            root_certificates=ca_chain,
            require_client_auth=config.require_client_auth
        )
        server = grpc.server(
            futures.ThreadPoolExecutor(max_workers=config.max_threads),
            interceptors=(auth.AuthTokenInterceptor(),)
        )
        port = server.add_secure_port(f'[::]:{config.port}', server_creds)
    else:
        server = grpc.server(
            futures.ThreadPoolExecutor(max_workers=config.max_threads),
        )
        port = server.add_insecure_port(f'[::]:{config.port}')
    metrics = MetricsStore(_DEFAULT_BUCKET_DURATION)
    network_map = Network(metrics)
    pb_grpc.add_NetworkDataServicer_to_server(NetworkDataServicer(network_map), server)
    pb_grpc.add_MeterReadingsServicer_to_server(MeterReadingsServicer(metrics, network_map), server)
    server.start()
    try:
        yield network_map, port
    finally:
        server.stop(0)


def run_grpc(conf):
    with serve(conf) as ret:
        network_map, port = ret
        log.info('Server is listening at port :%d', port)
        try:
            while True:
                time.sleep(_ONE_DAY_IN_SECONDS)
        except KeyboardInterrupt:
            pass


def main():
    parser = argparse.ArgumentParser(description="Network API server")
    parser.add_argument('--config', help='YAML Configuration file to use', default=CONFIG_FILE_LOCATIONS)
    args = parser.parse_args()
    conf = load_config(args.config)
    loop = asyncio.get_event_loop()
    t2 = threading.Thread(target=run_grpc, args=(conf,))
    t2.start()
    loop.run_until_complete(auth.start_conf_server())


if __name__ == '__main__':
    main()
