"""
Copyright 2019 Zeppelin Bend Pty Ltd
This file is part of postbox.

postbox is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

postbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with postbox.  If not, see <https://www.gnu.org/licenses/>.
"""


import logging
import os
from yaml import safe_load, YAMLError
from cerberus import Validator
from postbox import Singleton
logger = logging.getLogger(name=__name__)

# TODO: Handle these..
CONFIG_FILE_LOCATIONS = ["/etc/postbox/config.yml",
                         os.path.abspath(os.path.join(os.path.dirname(__file__), "default_config.yml"))]

CONFIG_SCHEMA_FILE = os.path.abspath(os.path.join(os.path.dirname(__file__), "config_schema.yml"))


class LogFormatter(logging.Formatter):
    def format(self, record):
        threadn = f'{record.threadName:10.10s}:{record.thread}'
        record.msg = f'{self.formatTime(record)} {threadn:16s} - {record.levelname:5s} - {record.filename}:{record.lineno} - {record.msg}'
        return super().format(record)


class InvalidConfigException(Exception):
    def __init__(self, message, errors):
        super().__init__(self, message)


def load_config(paths):
    """
    Loads a YAML configuration file. Only the first file found is loaded.
    :param paths: A list of paths to check for files, or a single path (str).
    :return: Dict representing the config file
    :raises: YAMLError if the found file contains errors. FileNotFoundError if we failed to locate any file
    """
    if isinstance(paths, str):
        paths = [paths]

    for path in paths:
        if os.path.isfile(path):
            try:
                with open(path, "r") as conf:
                    try:
                        config = safe_load(conf)
                        break
                    except YAMLError as y:
                        if hasattr(y, 'problem_mark'):
                            mark = y.problem_mark
                            logger.error("Configuration file at %s could not be loaded, error at %s:%s", os.path.abspath(path), mark.line+1, mark.column+1)
                        else:
                            logger.error("Configuration file at %s could not be loaded, is this a yaml file?", os.path.abspath(path))
                        raise y
            except FileNotFoundError as fe:
                logger.info("Configuration file at %s did not exist", os.path.abspath(path))
    else:
        logger.error("No configuration file was found, checked locations: %s", paths)
        raise FileNotFoundError("No configuration files found")

    validate_conf(config)
    return Config(**config)


def validate_conf(conf):
    """
    Validates a yaml config file. The "schema" for the configuration file is defined in
    config_schema.yml, and represents all the properties.
    :param conf:  A dict representing a configuration file to be validated
    :raises InvalidConfigException: If configuration couldn't be validated
    """
    with open(CONFIG_SCHEMA_FILE, "r") as f:
        schema = safe_load(f)
    v = Validator(schema)
    v.validate(conf, schema)
    if v.errors:
        logger.error("The configuration file contained errors:")
        for key, errors in v.errors.items():
            logger.error("%s was %s but had errors: %s", key, conf[key], errors)
        raise InvalidConfigException("YAML configuration could not be validated.", v.errors)


class Config(object, metaclass=Singleton):
    def __init__(self, ssl_enabled=True, require_client_auth=False, max_threads=10, port=50051, certificate=None,
                 key=None, rootca=None, jwks_url=None, pickle_path=None):
        self._ssl_enabled = ssl_enabled
        self._require_client_auth = require_client_auth
        self._max_threads = max_threads
        self._port = port
        self._cert = certificate
        self._key = key
        self._root_certificate = rootca
        self._jwks = jwks_url
        self._pickle_path = pickle_path

    @property
    def max_threads(self):
        return self._max_threads

    @max_threads.setter
    def max_threads(self, max_threads):
        self._max_threads = max_threads

    @property
    def ssl_enabled(self):
        return self._ssl_enabled

    @property
    def require_client_auth(self):
        return self._require_client_auth

    @property
    def jwks(self):
        return self._jwks

    @property
    def port(self):
        return self._port

    @property
    def cert(self):
        return self._cert

    @property
    def key(self):
        return self._key

    @property
    def pickle_path(self):
        return self._pickle_path

    @property
    def root_certificate(self):
        return self._root_certificate
