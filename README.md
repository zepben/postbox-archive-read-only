**WARNING: FOR DEVELOPMENT USE ONLY**

# Postbox
Postbox is an API server for storing, accessing, and tracing electricity distribution network models as per [Zepben's CIM profile](https://zepben.bitbucket.io/docs/cim/zepben/). This is work undertaken as part of the Evolve project and is currently under active development.

## Set up
The default config works out of the box for development purposes, and will bind the server (unsecured) to `localhost:50051`

For secure configuration see section [SSL](#SSL)

## Installation
Install is only supported for development purposes. In a virtualenv:

    pip install -e .

## Running the server
Simply running `main.py` will launch an insecure postbox server listening on 0.0.0.0:50051. This is not suitable for production.

    postbox

To pass in a configuration file run:

    postbox --config <path/to/postbox.yml>
    
A template configuration file can be found at [src/postbox/default_config.yml](src/postbox/default_config.yml)
The server will accept requests as per the postbox proto found in [cimproto](TODO)

## Security
### Authentication 
The server will interrogate all requests for an Access token (a Bearer token) and authenticate this token against the configured OAuth2 server. Scopes are specified for each RPC service.

### SSL
gRPC requires a signed certificate and key in order to operate on an SSL port. Hostname verification is also performed, and thus you will have to provide your hostname in the CN. You may also want to specify a Subject Alternative Name (SAN) with the hostname to comply with standards, however this is not required solely for the gRPC server.

#### Generating keys and certificates
TODO:

    openssl req -x509 -nodes -days 1000 -newkey rsa:2048 -keyout privateKey.key -out certificate.crt
    

