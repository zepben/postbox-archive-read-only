"""
Copyright 2019 Zeppelin Bend Pty Ltd
This file is part of postbox.

postbox is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

postbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with postbox.  If not, see <https://www.gnu.org/licenses/>.
"""


from pytest import raises, fixture
from postbox.network_services import NetworkDataServicer
from zepben.cim.iec61970 import PowerTransformer, PowerTransformerEnd, RatioTapChanger, EnergyConsumer, \
    EnergySource, AcLineSegment, PhaseShuntConnectionKind, EnergyConsumerPhase, SinglePhaseKind
from zepben.cim.iec61970 import PhaseCode, Terminal, DiagramObjectPoint, DiagramObject
from zepben.cim.iec61968 import PositionPoint, Location
from zepben.cim.iec61968 import MeterReading, Reading
from zepben.model import EquipmentContainer, MetricsStore
from google.protobuf.timestamp_pb2 import Timestamp

from postbox.metrics_services import MeterReadingsServicer

types = [ PowerTransformer, EnergySource, EnergyConsumer, AcLineSegment, Terminal ]


class FakeContext(object):
    def __init__(self):
        self.details = ""
        self.code = None

    def set_details(self, details):
        self.details = details

    def set_code(self, code):
        self.code = code


def get_test_network():
    metrics_store = MetricsStore(5000)
    network = EquipmentContainer(metrics_store)
    ns = NetworkDataServicer(network)
    # Energy Source
    t1 = Terminal(mRID="1:1", phases=PhaseCode.ABC, connectivityNode='node_1', sequenceNumber=1)
    pos = [PositionPoint(latitude=1.0, longitude=1.0)]
    loc = Location(position_points=pos)
    dpos = [DiagramObjectPoint(x=1.0, y=1.0)]
    diag_objs = [DiagramObject(mrid="a", points=dpos)]
    ns.recordEnergySource(EnergySource(mRID="1", inService=True, nominalVoltage=240, activePower=100, terminals=[t1], location=loc, diagramObjects=diag_objs), FakeContext())

    # Line
    t1 = Terminal(mRID="2:2", phases=PhaseCode.ABC, connectivityNode='node_1', sequenceNumber=1)
    t2 = Terminal(mRID="2:3", phases=PhaseCode.ABC, connectivityNode='node_2', sequenceNumber=2)
    pos = [PositionPoint(latitude=1.0, longitude=2.0)]
    loc = Location(position_points=pos)
    dpos = [DiagramObjectPoint(x=2.0, y=1.0)]
    diag_objs = [DiagramObject(mrid="a", points=dpos)]
    ns.recordACLineSegment(AcLineSegment(mRID="2", inService=True, nominalVoltage=240, length=10.0, r=5.0, x=5.0, x0=3.0,
                                         r0=3.0, terminals=[t1, t2], location=loc, diagramObjects=diag_objs, posPoints=[pos], diagramPoints=dpos, ratedCurrent=100.0), FakeContext())

    # Transformer
    t1 = Terminal(mRID="3:4", phases=PhaseCode.ABC, connectivityNode='node_2', sequenceNumber=1)
    t2 = Terminal(mRID="3:5", phases=PhaseCode.ABC, connectivityNode='node_3', sequenceNumber=2)
    pos = [PositionPoint(latitude=1.0, longitude=2.0)]
    loc = Location(position_points=pos)
    dpos = [DiagramObjectPoint(x=3.0, y=1.0)]
    diag_objs = [DiagramObject(mrid="a", points=dpos)]
    tapchanger1 = RatioTapChanger(highStep=10, lowStep=5, step=3.0, stepVoltageIncrement=20.0)
    tapchanger2 = RatioTapChanger(highStep=30, lowStep=10, step=3.0, stepVoltageIncrement=20.0)
    tend_1 = PowerTransformerEnd(ratedS=200.0, ratedU=250.0, r=33.3, x=50.2, r0=23.2, x0=35.3,
                                 connectionKind=PhaseShuntConnectionKind.D, terminals=[t1], location=loc, diagramObjects=diag_objs,
                                 tapChanger=tapchanger1, endNumber=1)
    tend_2 = PowerTransformerEnd(ratedS=300.0, ratedU=500.0, r=66.3, x=100.2, r0=43.2, x0=75.3,
                                 connectionKind=PhaseShuntConnectionKind.D, terminals=[t2], location=loc, diagramObjects=diag_objs,
                                 tapChanger=tapchanger2, endNumber=2)
    transformer = PowerTransformer(mRID="3", name="Transformer_up", inService=True, vectorGroup="DD0", terminals=[t1, t2],
                                   ends=[tend_1, tend_2], location=loc, diagramObjects=diag_objs)
    ns.recordPowerTransformer(transformer, FakeContext())

    # Line
    t1 = Terminal(mRID="4:6", phases=PhaseCode.ABC, connectivityNode='node_3', sequenceNumber=1)
    t2 = Terminal(mRID="4:7", phases=PhaseCode.ABC, connectivityNode='node_4', sequenceNumber=2)
    pos = [PositionPoint(latitude=1.0, longitude=3.0)]
    loc = Location(position_points=pos)
    dpos = [DiagramObjectPoint(x=4.0, y=1.0)]
    diag_objs = [DiagramObject(mrid="a", points=dpos)]
    ns.recordACLineSegment(AcLineSegment(mRID="4", inService=True, nominalVoltage=240, length=10.0, r=5.0, x=5.0, x0=3.0,
                                         r0=3.0, terminals=[t1, t2], location=loc, diagramObjects=diag_objs, ratedCurrent=100.0), FakeContext())
    # EnergyConsumer
    t1 = Terminal(mRID="5:8", phases=PhaseCode.ABC, connectivityNode='node_4', sequenceNumber=1)
    pos = [PositionPoint(latitude=1.0, longitude=4.0)]
    loc = Location(position_points=pos)
    dpos = [DiagramObjectPoint(x=5.0, y=1.0)]
    diag_objs = [DiagramObject(mrid="a", points=dpos)]
    ecp = EnergyConsumerPhase(pfixed=10.0, qfixed=20.0, phase=SinglePhaseKind.A)
    ns.recordEnergyConsumer(EnergyConsumer(mRID="5", name="singlephaseload", inService=True, nominalVoltage=240.0, p=10.0, q=20.0,
                                           terminals=[t1], location=loc, diagramObjects=diag_objs,
                                           phaseConnection=PhaseShuntConnectionKind.I, energyConsumerPhase=[ecp]), FakeContext())

    meter_servicer = MeterReadingsServicer(ns.network.metrics_store)
    # One meter, three different readings at timestamp 1 on a single phase load
    timestamp = Timestamp(seconds=1)
 #   reading_type = ReadingType(kind=MeasurementKind.voltage, phases=PhaseCode.A, unitSymbol=UnitSymbol.V)
 #   reading = Reading(timestamp=timestamp, value=30.3, type=reading_type)
 #   mr = MeterReading(mRID="1", name="meter_1", powerSystemResource="5", readings=[reading])
 #   meter_servicer.sendMeterReading(mr, FakeContext())

 #   reading_type = ReadingType(kind=MeasurementKind.power, phases=PhaseCode.A, unitSymbol=UnitSymbol.W)
 #   reading = Reading(timestamp=timestamp, value=100.3, type=reading_type)
 #   mr = MeterReading(mRID="1", name="meter_1", powerSystemResource="5", readings=[reading])
 #   meter_servicer.sendMeterReading(mr, FakeContext())

 #   reading_type = ReadingType(kind=MeasurementKind.power, phases=PhaseCode.A, unitSymbol=UnitSymbol.VA)
 #   reading = Reading(timestamp=timestamp, value=10.0, type=reading_type)
 #   mr = MeterReading(mRID="1", name="meter_1", powerSystemResource="5", readings=[reading])
 #   meter_servicer.sendMeterReading(mr, FakeContext())
    return ns, meter_servicer

# TODO: figure out how to break this into two fixtures.. preferably from the same source? although maybe it doesn't matter
#       because the data is always going to be the same?
@fixture()
def network():
    ns, ms = get_test_network()
    yield ns, ms


