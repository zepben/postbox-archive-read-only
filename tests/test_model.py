"""
Copyright 2019 Zeppelin Bend Pty Ltd
This file is part of postbox.

postbox is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

postbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with postbox.  If not, see <https://www.gnu.org/licenses/>.
"""


from pytest import raises
from zepben.model import PowerTransformer, PowerTransformerEnd, RatioTapChanger, EnergyConsumer, EnergySource, \
    ACLineSegment, Terminal, PositionPoint, Location, DiagramObject, DiagramObjectPoint, InvalidTransformerError, ConnectivityNode
from zepben.cim.iec61970.base.core.PhaseCode_pb2 import PhaseCode
from zepben.cim.iec61970.base.wires.PhaseShuntConnectionKind_pb2 import PhaseShuntConnectionKind


# TODO:
# Test adding a terminal multiple times to the same node (shouldn't occur)
# Test ejson outputs highest v_base for a node
# test longlat is set in ejson node
# Test node with multiple equipment: nominal_voltage not set on some equipment - ejson should still output max nom_volt across all equip.
# Test node with multiple equipment but no v_base set - should default to 0.0

# Test EJSON conforms to model - i.e, load in ejson and compare it to the model (i guess that implies transforming it back?)
# Test EJSON 3-phase measurements have 3 data points in 1 measurement.
# Test EJSON xy and longlat points - missing versus set versus default?


# Test converting to protobuf

class TestModel(object):

    def test_convert_es_to_pb_es(self):
        cn = ConnectivityNode(mrid="node_1")
        t1 = Terminal(mrid="1:1", phases=PhaseCode.ABC, connectivity_node=cn)
        cn.add_terminal(t1)
        pos = [PositionPoint(latitude=1.0, longitude=1.0)]
        loc = Location(position_points=pos)
        dpos = [DiagramObjectPoint(x=1.0, y=1.0)]
        diag_objs = [DiagramObject(mrid="a", points=dpos)]
        es = EnergySource("1", 100.0, 3.2, 6.8, 415.0, 32.0, 100.0,
                          200.0, name="FakeSource", in_service=True, terminals=[t1], diag_objs=diag_objs, location=loc)
        pb = es.to_pb()
        assert pb.mRID == "1"
        assert pb.activePower == 100.0
        assert pb.r == 3.2
        assert pb.x == 6.8
        assert pb.nominalVoltage == 415.0
        assert pb.reactivePower == 32.0
        assert pb.voltageAngle == 100.0
        assert pb.voltageMagnitude == 200.0
        assert pb.name == "FakeSource"
        assert pb.inService
        assert pb.terminals[0].mRID == "1:1"
        assert pb.terminals[0].phases == PhaseCode.ABC
        assert pb.terminals[0].connectivityNode == "node_1"
        assert pb.diagramObjects[0].diagramObjectPoints[0].xPosition == dpos[0].x_position
        assert pb.diagramObjects[0].diagramObjectPoints[0].yPosition == dpos[0].y_position
        assert pb.location.posPoints[0].xPosition == pos[0].x_position
        assert pb.location.posPoints[0].yPosition == pos[0].y_position

    def test_duplicate_transformer_ends(self):
        pt = PowerTransformer(mrid="1")
        end1 = PowerTransformerEnd(end_number=1)
        end2 = PowerTransformerEnd(end_number=2)
        end3 = PowerTransformerEnd(end_number=1)
        pt.add_end(end1)
        pt.add_end(end2)
        assert pt.endCount == 2
        with raises(InvalidTransformerError):
            pt.add_end(end3)

