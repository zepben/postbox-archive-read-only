"""
Copyright 2019 Zeppelin Bend Pty Ltd
This file is part of postbox.

postbox is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

postbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with postbox.  If not, see <https://www.gnu.org/licenses/>.
"""


from glob import glob
from os.path import basename
from os.path import splitext
from setuptools import setup, find_packages
from setuptools.command.build_py import build_py as build_py_orig


class BuildWithProto(build_py_orig):

    def run(self):
        self.build_proto()

    def build_proto(self):
        """This will build the pb2 files at pip install time"""
        import grpc_tools.protoc

        result = grpc_tools.protoc.main([
            'grpc_tools.protoc',
            '-Iprotos',
            '--python_out=src/postbox/',
            '--grpc_python_out=src/postbox/',
            'protos/network.proto'
        ])

        if result:
            raise Exception(f'protoc failed with {result}')


test_deps = ["pytest"]
setup(
    name="postbox",
    version="0.1",
    packages=find_packages('src'),
    package_dir={'': 'src'},
    py_modules=[splitext(basename(path))[0] for path in glob('src/*.py')],
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'postbox = postbox.main:main'
        ]
    },
    install_requires=[
        "zepben.cimbend",
        "Pyyaml",
        "cerberus",
        "grpcio",
        "aiohttp",
        "aiodns",
        "python-jose-cryptodome",
        "requests",
        "grpcio-tools"
    ],
    extras_require={
        "test": test_deps,
    },
    cmdclass={
        'build_ext': BuildWithProto
    }
)
